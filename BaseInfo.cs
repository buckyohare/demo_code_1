﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseInfo : MonoBehaviour {


	public int PlayerIndex;
	public int Loop;
	public int LoopIndex;
	public int Pitch;
	public float Height;
	public float Width;

	public float HeightMod;

	public List<ActiveBases> thisBase = new List<ActiveBases>();

	private PlayerSetupDefinition player;


	void Start (){

	}

	void Update ()
	{
		Height = gameObject.transform.localScale.y;
		Width = gameObject.transform.localScale.x;

	}

	void OnDestroy()
	{

	}

}
