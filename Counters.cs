﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Counters {

	public List<int> Loop;

	public Counters (List<int> newLoop){
		Loop = newLoop;
	}

}
