﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class PlayerSetupDefinition  {


	public string Name;

	public List<BasesAndLoops> BasesAndLoops = new List<BasesAndLoops>();

	public Transform Location;

	public Color AccentColor;

	public List<GameObject> StartingUnits = new List<GameObject>();

	private List<GameObject> activeUnits = new List<GameObject> ();
	public List<GameObject> ActiveUnits { get { return activeUnits; } }

	public List<Color> BlinkColours = new List<Color>();

	public bool IsAi;

	public float Resources;

	public float RemainingPool = 20;

	public List<ActiveBases> ActiveBases = new List<ActiveBases> ();

	private List<GameObject> droneList = new List<GameObject>();
	public List<GameObject> DroneList { get { return droneList; } }

	public int PitchCellSize;

	public GameObject notePrefab;

	public GameObject CentoidMarker;
	public GameObject BaseCentrePoint;
	public GameObject SpreadMarker;
	public GameObject LoopAreaMarker;
	public GameObject ResourceCloud;

	private List<float> droneDistances = new List<float>();
	public List<float> DroneDistances { get { return droneDistances; } }

}
