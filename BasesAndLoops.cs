﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class BasesAndLoops {

	public int LoopLength;
	public int MaxBasesInThisLoop;
	public float BuildHereWeight;
	public List<ActiveBases> ActiveBases;

	public float DistroDistance;
	public List<int> Events = new List<int>();
	public List<int> EIndexes = new List<int>();
	public List<int> DistroWeights = new List<int>();
	public List<float> AdjustedDistroWeights = new List<float>();

	public List<float> Pitches = new List<float> ();

	public List<float> HeightWeights = new List<float>();

	public float[] Durations;
	public List<float> DurationWeights = new List<float>();
	public List<float> AdjustedDurationWeights = new List<float>();

	public GameObject BasePrefab;
	public Transform LoopLocation;
	public GameObject LoopAreaMarker;

	// 0 = close, 100 = far, 50 means equal probability

	// also rules could go here...

	public BasesAndLoops (int newLoopLength, GameObject newBasePrefab, Transform newLoopLocation, GameObject newLoopAreaMarker, int newMaxBasesInThisLoop, List<ActiveBases> newActiveBases, float newBuildHereWeight)
	{
		LoopLength = newLoopLength;
		BasePrefab = newBasePrefab;
		LoopLocation = newLoopLocation;
		LoopAreaMarker = newLoopAreaMarker;
		MaxBasesInThisLoop = newMaxBasesInThisLoop;
		ActiveBases = newActiveBases;
		BuildHereWeight = newBuildHereWeight;
//		HeightMods = newHeightMods;

	}


}
