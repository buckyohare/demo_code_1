﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class Sound_Metronome : MonoBehaviour {

	public OSCHandler oschandle;

	public double bpm = 140.0F;
	public float gain = 0.5F;
	public int signatureHi = 4;
	public int signatureLo = 4;
	private double nextTick = 0.0F;
	private float amp = 0.0F;
	private float phase = 0.0F;
	private double sampleRate = 0.0F;
	private int accent;
	private bool running = false;

	public List<Counters> Counts = new List<Counters> ();

	public bool playFX1 = false;
	public bool playFX2 = false;
	public bool playFX3 = false;
	public bool playFX4 = false;


	void Start () {

		for (var p = 0; p < RtsManager.Current.Players.Count; p++) {
			List <int> playersLoops = new List<int> ();
			Counts.Add (new Counters (playersLoops));
			for (var l = 0; l < RtsManager.Current.Players[p].BasesAndLoops.Count; l++){
				Counts [p].Loop.Add (1);
			}
		}

		accent = signatureHi;
		double startTick = AudioSettings.dspTime;
		sampleRate = AudioSettings.outputSampleRate;
		nextTick = startTick * sampleRate;
		running = true;

		OSCHandler.Instance.Init();

	}



	void OnAudioFilterRead(float[] data, int channels)
	{
//		Debug.Log ("tick");
		if (!running)
			return;

		double samplesPerTick = sampleRate * 60.0F / bpm * 4.0F / signatureLo;
		double sample = AudioSettings.dspTime * sampleRate;
		int dataLen = data.Length / channels;
		int n = 0;
		while (n < dataLen)
		{
			float x = gain * amp * Mathf.Sin(phase);
			int i = 0;
			while (i < channels)
			{
				data[n * channels + i] += x;
				i++;
			}
			while (sample + n >= nextTick)
			{
				nextTick += samplesPerTick;
				amp = 1.0F;
				if (++accent > signatureHi)
				{
					accent = 1;
					amp *= 2.0F;

				}
				// new counter!
				int directory = 5;
				for (var c = 0; c < RtsManager.Current.Players.Count; c++){
					var player = RtsManager.Current.Players [c];
					for (var l = 0; l < player.BasesAndLoops.Count; l++){
						for (var z = 0; z < player.BasesAndLoops[l].ActiveBases.Count; z++)
						{
							// for each active base, check the loop index, if it's the same as this count, send the pitch etc. to the corresponding osc address.
							if (player.BasesAndLoops [l].ActiveBases [z].LoopIndex == Counts [c].Loop [l]) {
								List<float> noteDeets = new List<float> ();
								noteDeets.Add(player.BasesAndLoops [l].ActiveBases [z].Pitch);
								noteDeets.Add(player.BasesAndLoops [l].ActiveBases [z].Width);
								noteDeets.Add(player.BasesAndLoops [l].ActiveBases [z].Height);
								OSCHandler.Instance.SendMessageToClient("MaxMSP", "/" + directory + "/", noteDeets);
							}
						}
						directory++;

						// add to the counters
						if (Counts [c].Loop [l] >= player.BasesAndLoops [l].LoopLength) {
							Counts [c].Loop [l] = 1;
						} else {
							Counts [c].Loop [l]++;
						}
					}
				}

				if (playFX1) {
					OSCHandler.Instance.SendMessageToClient("MaxMSP", "/1/", 1);
					playFX1 = false;
				}
				if (playFX2) {
					OSCHandler.Instance.SendMessageToClient("MaxMSP", "/1/", 2);
					playFX2 = false;
				}
				if (playFX3) {
					OSCHandler.Instance.SendMessageToClient("MaxMSP", "/1/", 3);
					playFX3 = false;
				}
				if (playFX4) {
					OSCHandler.Instance.SendMessageToClient("MaxMSP", "/1/", 4);
					playFX4 = false;
				}


			}
			phase += amp * 0.3F;
			amp *= 0.993F;
			n++;
		}
	}

}
