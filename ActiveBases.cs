﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class ActiveBases {

	public GameObject BaseObject;
	public int Loop;
	public int LoopIndex;
	public int Pitch;
	public float Height;
	public float Width;

	public float HeightMod;


	public ActiveBases (GameObject newBaseObject, int newLoop, int newLoopIndex, int newPitch, float newHeigh, float newWidth, float newHeightMod)
	{
		BaseObject = newBaseObject;
		Loop = newLoop;
		LoopIndex = newLoopIndex;
		Pitch = newPitch;
		Height = newHeigh;
		Width = newWidth;
		HeightMod = newHeightMod;

	}

}
